# How to run details service

## Prerequisite

* Ruby 2.7

```bash
ruby details.rb 9080
```

## test in dokcer
``` 
docker build -t details .  --no-cache
docker run -p 9080:9080 details
```